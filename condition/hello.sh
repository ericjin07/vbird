#!/bin/bash

echo "the file is ${0}"
if [ "${1}" == "" ]; then
	echo "you must input some param"
	exit 0
elif [ "${1}" != "hello" ];then
	echo "you must use hello as the parameter"
	exit 0
else
	echo "hello,how are you?"
	exit 0
fi

