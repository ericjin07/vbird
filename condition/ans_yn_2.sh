#!/bin/bash
# yes or no
echo -e 'please input your choice!'
read -p "do you like me?(y/n)" choice
if [ "${choice}" == "y" -o ${choice} == "Y" ]; then
       	echo "ok, continue!" 
       	exit 0
fi
if [ "${choice}" == "n" ] || [ ${choice} == "N" ]; then
       	echo "oh, interrupt!" 
       	exit 0
fi
echo "I don't know what your choice are"
exit 0
