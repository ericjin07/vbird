#!/bin/bash
echo -e "Please input a filename,I'll check the filename's type and permission.\n\n"
# 1.判读用户是否输入了有效的字串
read -p "input a filename: " filename
test -z ${filename} && echo "you mush input a filename" && exit 0
# 2.判断文件是否存在
test ! -e ${filename} && echo "the file ${filename} DO NOT EXIST" && exit 0
# 3.判断文件类型和属性
test -f ${filename} && filetype="regular file"
test -d ${filename} && filetype="directory"
test -r ${filename} && perm="readable"
test -w ${filename} && perm="${perm} writable"
test -x ${filename} && perm="${perm} executable"
# 4.开始输出咨询
echo "the file ${filename} is a ${filetype}"
echo "the file's permission for you are: ${perm}"
