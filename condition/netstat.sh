#!/bin/bash
echo "I'll detect your linux server's services"
echo -e "The www, ssh , ftp, mail will be detected\n"
testfile=/home/eric/vbird/netstat.log
netstat -tunl > ${testfile}
testing=$(grep ":80" ${testfile})
if [ "${testing}" != "" ];then
	echo "WWW is running in your system"
fi
testing=$(grep ":22" ${testfile})
if [ "${testing}" != "" ];then
	echo "ssh is running in your system"
fi
testing=$(grep ":21" ${testfile})
if [ "${testing}" != "" ];then
	echo "ftp is running in your system"
fi
testing=$(grep ":25" ${testfile})
if [ "${testing}" != "" ];then
	echo "mail is running in your system"
fi
