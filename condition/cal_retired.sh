#!/bin/bash
#1.先让用户输入退伍日期
echo "This program will try to calculate: "
echo "Homw many days before you demobilization date.."
read -p "Please input your demobilization date yyyyMMdd(20180718): " dateDemo
#2.比较现在和退伍日期
date_test=$(echo ${dateDemo} | grep '[0-9]\{8\}')
if [ "${date_test}" == "" ];then
	echo -e "you input the wrong date format!"
	exit 1
fi

declare -i date_dem=$(date --date="${dateDemo}" +%s)	#the demobilization time
declare -i date_now=$(date +%s)	#now
declare -i total=$((${date_dem} - ${date_now}))
declare -i date_d=$((${total}/60/60/24))	#change to how many days
#3.由两个日期来显示【还需要几天】的退伍字样
if [ "${total}" -lt "0" ]; then
	echo "You had been demobilization before: " $((-1*${date_d})) "days ago"
else
	echo "You will demobilize after ${date_d} days!"
fi

