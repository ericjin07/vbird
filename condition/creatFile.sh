#! /bin/bash
echo -e "I'll use 'touch' command to create three files"
read -p "Please input the filename: " fileuser

# to void the Enter input,user varian function to default setting
filename=${fileuser:-"filename"}

#use date to modify the defalut filename
date1=$(date --date='2 days ago' +%Y%m%d)
date2=$(date --date='1 days ago' +%Y%m%d)
date3=$(date +%Y%m%d)
file1=${filename}${date1}
file2=${filename}${date2}
file3=${filename}${date3}

#create files
touch ${file1}
touch ${file2}
touch ${file3}
